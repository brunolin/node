var brnApp = angular.module('brnApp', ['ngResource', 'ngRoute', 'ngAlertify']);


brnApp.config(['$routeProvider', function($routeProvider){
    
    $routeProvider.when('/:id', {
        templateUrl: function(params){
            return '../pages/' + params.id + '.html'
        }
    });
}]);

brnApp.controller('brnCtrl', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

    window.location = '#/servico';
    $scope.logoName = 'LF Antenas';
    $scope.menuList = [
        {field:'Serviços', id:'servico'},
        {field:'Orçamento', id:'orcamento'}
    ];

    $scope.selected = function selected(active){
        if(active != undefined){
            $scope.menu = active;
        }
        return $scope.menu;
    };
    $scope.menu = 'servico';
}]);

brnApp.controller('servicoCtrl', ['$scope', '$http', 'Rest', function($scope, $http, Rest){
    
    $scope.services = Rest.getServices();
}]);

brnApp.controller('orcamentoCtrl', ['$scope', '$http', 'Rest', 'alertify', 'Rest', function($scope, $http, Rest, alertify, Rest){
    
    $scope.contacts = Rest.getContacts();

    $scope.email = {};
    $scope.sendingEmail = false;

    $scope.sendEmail = function sendEmail() {

        $scope.sendingEmail = true;
        $scope.email.name = $scope.email.firstName + ' ' + $scope.email.lastName;

        Rest.sendEmail($scope.email).$then(function(){
            alertify.success('Enviado com sucesso');
            $scope.sendingEmail = false;
            $scope.email = {};
        });
        // $http.post('/r/email', $scope.email).then(function(){
        //     alertify.success('Enviado com sucesso');
        //     $scope.sendingEmail = false;
        //     $scope.email = {};
        // });
    };
}]);


brnApp.factory('Rest', ['$resource', function ($resource) {

    return $resource('/r/:action', {}, {
        'getContacts': {
            method: 'GET',
            isArray: true,
            params: { action: 'contacts' }
        },
        'getServices': {
            method: 'GET',
            isArray: true,
            params: { action: 'services' }
        },
        'sendEmail': {
            method: 'POST',
            isArray: false,
            params: { action: 'email' }
        }
    });
}]);











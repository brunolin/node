var controller = require('../controllers/contato.js')();

module.exports = function(app) {
	app.post('/r/email', controller.sendMail);
	app.get('/r/contacts', controller.getContacts);
	app.get('/r/services', controller.getServices);
};
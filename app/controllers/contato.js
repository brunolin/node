module.exports = function(){

	var controller = {};

	controller.sendMail = function(req, res){
		var nodemailer = require('nodemailer');
		var transporter = nodemailer.createTransport('smtps://brunolinteste@gmail.com:Brunolin!@smtp.gmail.com');

		var mailOptions = {
		    from: req.body.name + ' <' + req.body.from + '>',
		    to: 'brunolinteste@gmail.com',
		    subject: 'Orçamento',
		    text: req.body.description,
		    html: '<div><span>Nome: ' + req.body.name + '</span></div>' + '<div><span>Email: ' + req.body.from + '</span></div>' + '<div><span>Serviço: ' + req.body.service + '</span></div>' + '<p>' + req.body.description +'</p>'
		};
		transporter.sendMail(mailOptions, function(error, info){
		    if(error){
		        return console.log(error);
		    }
		    console.log('Message sent: ' + info.response);
		    res.status(200).send();
		});
	};

	controller.getContacts = function(req, res){

		var contacts = [
	        {number: '(41)9572-3883'},
	        {number: '(41)9281-9121'}
    	];

    	res.send(contacts);
	};

	controller.getServices = function(req, res) {
		
		var services = [
	        {label:'Instalações de antenas'},
	        {label:'UHF/VHF/Digital'},
	        {label:'Apontamento para qualquer satélite'},
	        {label:'Manutenções'},
	        {label:'Fazemos instalação Sky livre'}
	    ];

	    res.send(services);
	}
	return controller;
}